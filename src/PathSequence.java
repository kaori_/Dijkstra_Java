import java.util.ArrayList;

class PathSequence {
    private ArrayList<Node> sequence;

    PathSequence() {
        sequence = new ArrayList<>();
    }

    void print() {
        for (Node node : sequence) {
            if (node.getValue() == ConstantValues.INFINITY) {
                System.out.println("I cannot find the path...");
                return;
            }
            System.out.print(node.getName() + " ");
        }
        System.out.println();
    }

    void stack(Node node) {
        sequence.add(0, node);
    }

    Node getLastNode() {
        if (sequence.size() != 0) {
            return sequence.get(sequence.size() - 1);
        } else {
            return null;
        }
    }
}
