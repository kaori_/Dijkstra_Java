public class Node {
    private String name;
    private int value;
    private String fromLabel;
    private boolean visitedFlag;

    Node(String name) {
        this.name = name;
        value = ConstantValues.INFINITY;
        fromLabel = null;
        visitedFlag = false;
    }

    String getName() {
        return name;
    }

    int getValue() {
        return value;
    }

    void setValue(int value) {
        this.value = value;
    }

    String getFromLabel() {
        return fromLabel;
    }

    void setFromLabel(String fromLabel) {
        this.fromLabel = fromLabel;
    }

    boolean isVisitedFlag() {
        return visitedFlag;
    }

    void makeVisited() {
        visitedFlag = true;
    }

    public String toString() {
        return "Name: " + name + ", Value: " + value + ", From: " + fromLabel + ", Visited?: " + visitedFlag;
    }
}
