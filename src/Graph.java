import java.util.ArrayList;

class Graph {
    private ArrayList<Node> nodes;
    private ArrayList<Edge> edges;

    Graph(ArrayList<Node> nodes, ArrayList<Edge> edges) {
        this.nodes = nodes;
        this.edges = edges;
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }
}
