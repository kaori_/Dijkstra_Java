class ShortestPath {
    private PathSequence pathSequence;
    private int totalCost;

    ShortestPath(PathSequence pathSequence) {
        this.pathSequence = pathSequence;
        if (pathSequence.getLastNode() != null) {
            totalCost = pathSequence.getLastNode().getValue();
        }
    }

    PathSequence getPathSequence() {
        return pathSequence;
    }

    int getTotalCost() {
        return totalCost;
    }
}
