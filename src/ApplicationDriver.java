import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Objects;

public class ApplicationDriver {

    public static void main(String[] args) {
        String source = "", destination = "";

        InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        System.out.print("Input the source: ");
        try {
            source = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print("Input the destination: ");
        try {
            destination = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<Node> nodes = createNodes(getNodesInput());
        ArrayList<Edge> edges = createEdges(getEdgesInput(), nodes);

        Node sourceNode = findNodeFromName(nodes, source);
        Node destinationNode = findNodeFromName(nodes, destination);

        if (sourceNode == null || destinationNode == null) {
            System.out.println("I think your inputs are wrong!");
            return;
        }

        Graph graph = new Graph(nodes, edges);
        Dijkstra dijkstra = new Dijkstra(sourceNode, destinationNode, graph);
        ShortestPath shortestPath = dijkstra.run();

        shortestPath.getPathSequence().print();
        if (shortestPath.getTotalCost() != ConstantValues.INFINITY) {
            System.out.println("Cost: " + shortestPath.getTotalCost());
        }
    }

    private static ArrayList<Node> createNodes(String userInput) {
        ArrayList<Node> result = new ArrayList<>();

        String[] nodeNames = userInput.split(",");
        for (String nodeName : nodeNames) {
            result.add(new Node(nodeName));
        }

        return result;
    }

    private static ArrayList<Edge> createEdges(String userInput, ArrayList<Node> nodes) {
        final int SOURCE_NAME = 0;
        final int DESTINATION_NAME = 1;
        final int COST = 2;

        ArrayList<Edge> result = new ArrayList<>();

        String[] edgeDefinitions = userInput.split(",");
        for (String edgeDefinition : edgeDefinitions) {
            String[] edgeString = edgeDefinition.split("_");
            Node sourceNode = findNodeFromName(nodes, edgeString[SOURCE_NAME]);
            Node destinationNode = findNodeFromName(nodes, edgeString[DESTINATION_NAME]);
            result.add(new Edge(sourceNode, destinationNode, Integer.parseInt(edgeString[COST])));
        }

        return result;
    }

    private static Node findNodeFromName(ArrayList<Node> nodes, String name) {
        for (Node node : nodes) {
            if (Objects.equals(name, node.getName())) return node;
        }
        return null;
    }

    private static String getNodesInput() {
        return "A,B,C,D,E,F,G"; //these are the nodes of the graph!
    }

    private static String getEdgesInput() {
//         return "A_B_10,A_F_6,B_C_6,C_B_10,D_A_3,E_B_5,F_B_10,G_G_0";
        return "A_B_10,A_F_6,B_C_6,C_B_10,D_A_3,E_B_5,G_B_3,F_B_10";
    }
}
