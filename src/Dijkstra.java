import java.util.ArrayList;
import java.util.Objects;

class Dijkstra {
    private Node source;
    private Node destination;
    private Graph graph;
    private PathSequence pathSequence;

    Dijkstra(Node source, Node destination, Graph graph) {
        this.source = source;
        this.destination = destination;
        this.graph = graph;
        pathSequence = new PathSequence();
    }

    ShortestPath run() {
        Node currentNode = source;
        for (Node node : graph.getNodes()) {
            if (Objects.equals(node.getName(), currentNode.getName())) {
                node.setValue(0);
                node.makeVisited();
            }
        }

        while (hasUnVisitedNode(graph.getNodes())) {
            ArrayList<Edge> targetEdges = getDependentEdges(currentNode.getName());

            for (Edge targetEdge : targetEdges) {
                targetEdge.update();
            }

            currentNode = getMinimumValueNode(graph.getNodes());
            currentNode.makeVisited();
        }

        Node reverseDetection = findNodeFromName(graph.getNodes(), destination.getName());
        while (reverseDetection != null) {
            pathSequence.stack(reverseDetection);
            reverseDetection = findNodeFromName(graph.getNodes(), reverseDetection.getFromLabel());
        }

        return new ShortestPath(pathSequence);
    }

    private ArrayList<Edge> getDependentEdges(String sourceNodeName) {
        ArrayList<Edge> result = new ArrayList<>();

        for (Edge edge : graph.getEdges()) {
            if (Objects.equals(edge.getSource().getName(), sourceNodeName)) {
                result.add(edge);
            }
        }

        return result;
    }

    private Node getMinimumValueNode(ArrayList<Node> nodes) {
        Node minimumNode = null;

        for (Node node : nodes) {
            if (node.isVisitedFlag()) continue;

            if (minimumNode == null || node.getValue() < minimumNode.getValue()) {
                minimumNode = node;
            }
        }

        return minimumNode;
    }

    private boolean hasUnVisitedNode(ArrayList<Node> nodes) {
        ArrayList<Node> unSearchedTargets = new ArrayList<>();
        for (Node node : nodes) {
            if (!node.isVisitedFlag()) unSearchedTargets.add(node);
        }
        return unSearchedTargets.size() != 0;
    }

    private static Node findNodeFromName(ArrayList<Node> nodes, String name) {
        for (Node node : nodes) {
            if (Objects.equals(name, node.getName())) return node;
        }
        return null;
    }
}
