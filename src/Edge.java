public class Edge {
    private Node source;
    private Node destination;
    private int cost;

    Edge(Node source, Node destination, int cost) {
        this.source = source;
        this.destination = destination;
        this.cost = cost;
    }

    Node getSource() {
        return source;
    }

    Node getDestination() {
        return destination;
    }

    int getCost() {
        return cost;
    }

    public String toString() {
        return "Source: " + source.getName() + ", Destination: " + destination.getName() + ", Cost: " + cost;
    }

    void update() {
        if (destination.getValue() <= cost + source.getValue()) return;

        destination.setValue(cost + source.getValue());
        destination.setFromLabel(source.getName());
    }
}
